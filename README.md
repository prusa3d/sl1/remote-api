## Rest api compatible with OctoPrint.

![pipeline](https://gitlab.com/prusa3d/sl1/remote-api/badges/master/pipeline.svg)
![coverage](https://gitlab.com/prusa3d/sl1/remote-api/badges/master/coverage.svg)
[Documentation](https://prusa3d.gitlab.io/sl1/remote-api)

### Summary

* [Run Dev](#run-dev)

### Run Dev
```bash
python3 -m venv venv
source ./venv/bin/activate
pip install -r requirements.txt
./scripts/dev.sh
```
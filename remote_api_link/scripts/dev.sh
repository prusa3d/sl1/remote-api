#!/bin/sh

# This file is part of the SLA firmware
# Copyright (C) 2020 Prusa Research a.s. - www.prusa3d.com
# SPDX-License-Identifier: GPL-3.0-or-later

PYTHON="$($SHELL which python)"
export FLASK_APP=remote_api_link/main_api.py
export FLASK_ENV=development
export MOCK_DBUS_LEVEL=DEBUG

PYTHONPATH="$(pwd)$(find ../dependencies/ -maxdepth 1 -type d -printf ':%p')"
export PYTHONPATH

trap "exit" INT TERM
trap "kill 0" EXIT

# $PYTHON tests/remote_api_link_mock/main.py 2>/dev/null &
$PYTHON -m flask run --port 8080

wait $!

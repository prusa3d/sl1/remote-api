#!/bin/sh

# This file is part of the SLA firmware
# Copyright (C) 2020 Prusa Research a.s. - www.prusa3d.com
# SPDX-License-Identifier: GPL-3.0-or-later

# use:
# tests: ./scripts/test.sh
# tests and save coverage: ./scripts/test.sh --cov
#

case "$1" in
    "--cov")
        ARGS="--cov=remote_api_link --cov-report term --cov-report html";;
    *)
        ARGS="";;
esac

PYTHON="$($SHELL which python)"
export FLASK_APP=remote_api_link/main_api.py
export FLASK_ENV=development
export MOCK_DBUS_LEVEL=INFO
export EXAMPLES_PATH="tests/examples"
PYTHONPATH="$(pwd)$(find ../dependencies/ -maxdepth 1 -type d -printf ':%p')"
export PYTHONPATH

if [ ! -d $EXAMPLES_PATH ] 
then
    tmp=$(mktemp --directory --tmpdir=/tmp/ examples.XXXX)
    echo "Local temp is ${tmp}"
    mkdir -p $EXAMPLES_PATH/media/sdb
    mkdir $EXAMPLES_PATH/projects
    wget -P ${tmp} https://sl1.prusa3d.com/examples.tar.gz
    tar -xzf ${tmp}/examples.tar.gz -C $EXAMPLES_PATH/media/sdb
    tar -xzf ${tmp}/examples.tar.gz -C $EXAMPLES_PATH/projects
    rm -fr ${tmp}
fi

file=$(find $EXAMPLES_PATH/projects -name "*.sl1" -print -quit)
echo "teste file: $file"
EXAMPLES_FILE="$file"
export EXAMPLES_FILE

$PYTHON tests/remote_api_link_mock/main.py 2>/dev/null &
DBUS=$!
trap "kill -9 ${DBUS}" EXIT
$PYTHON -m pytest $ARGS

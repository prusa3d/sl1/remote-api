# -*- coding: utf-8 -*-
# This file is part of the SLA firmware
# Copyright (C) 2021 Prusa Research a.s. - www.prusa3d.com
# SPDX-License-Identifier: GPL-3.0-or-later

import os
import tempfile
from pathlib import Path

DEBUG_FILE = "/etc/debug"
localedir = os.path.join(os.path.dirname(__file__), "locales")
TMP_FOLDER = os.path.join(tempfile.gettempdir(), "projects_metadata")
HEADER_TIME_FORMAT = "%a, %d %b %Y %X GMT"
NOT_MODIFIED = ("Not Modified", 304)
NO_CONTENT = ("No Content", 204)
MIN_SPACE_LEFT = 110 * 1024 * 1024  # 110MB

CREATED_JSON = ({}, 201)
NO_CONTENT_JSON = ({}, 204)
OK_JSON = ({}, 204)
NOT_FOUND_JSON = ({}, 404)
BAD_REQUEST_JSON = ({}, 400)

del os
del Path

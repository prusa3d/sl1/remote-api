# -*- coding: utf-8 -*-
# This file is part of the SLA firmware
# Copyright (C) 2021 Prusa Research a.s. - www.prusa3d.com
# SPDX-License-Identifier: GPL-3.0-or-later

import json
from string import Template

from flask import current_app, request
from werkzeug.exceptions import HTTPException
from werkzeug.wrappers.response import Response

from prusaerrors.shared.codes import Code
from prusaerrors.sl1.codes import Sl1Codes

from remote_api_link.states import State

ERROR_TEMPLATE = """
<!DOCTYPE html>
<html>
  <head>
    <style>
      html,
      a {
        background-color: #0a0a0a;
        font-size: 16px;
        color: #ffffff;
        font-family: Helvetica, sans-serif;
      }

      .title {
        display: flex;
        width: 100%;
        justify-content: center;
        align-items: center;
        border-bottom: 1px solid #707070;
      }
      img {
        padding-left: 22px;
      }
      .wrapper {
        margin: 25px;
      }
      .wrapper div {
        float: right;
        color: #ffffff;
      }
    </style>
  </head>
  <body>
    <div class="title">
      <h1>$title</h1>
      <img
        src="data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='34.654' height='34.658' viewBox='0 0 247.17 247.17' shape-rendering='geometricPrecision' image-rendering='optimizeQuality' fill-rule='evenodd' xmlns:v='https://vecta.io/nano'%3E%3Cpath d='M123.58 189.05c-4.11 0-7.84-1.67-10.54-4.37-2.71-2.71-4.38-6.44-4.38-10.55 0-4.1 1.67-7.83 4.38-10.54 2.69-2.7 6.43-4.38 10.54-4.38a14.89 14.89 0 0 1 10.55 4.37c2.7 2.71 4.37 6.44 4.37 10.54 0 4.11-1.67 7.84-4.38 10.54-2.69 2.7-6.43 4.38-10.54 4.38zM73.33 0h101.44l72.39 72.39v102.39l-72.39 72.39H72.39L0 174.77V72.4L72.39.01h.95zm149.05 82.66L164.5 24.78H82.66L24.78 82.66v81.85l57.88 57.88h81.84l57.88-57.88V82.66zM113.47 60.65h22.51v85.43h-24.79V60.65h2.28z' fill='%23fff'/%3E%3C/svg%3E"
      />
    </div>
    <div class="wrapper">
      <p>$code</p>
      <p>$description</p>
      <div>
        $link_desc:
        <a href="$link" target="_blank"
          >$link</a
        >
      </div>
    </div>
  </body>
</html>
"""  # noqa: E501


def create_error_page(
    title=None, code="", description="", link="https://help.prusa3d.com"
):
    if title is None:
        title = _("NO ERROR")
    return " ".join(
        Template(ERROR_TEMPLATE)
        .substitute(
            title=title,
            code=code,
            description=description,
            link_desc=_("More info:"),
            link=link,
        )
        .split()
    )


def with_code(code: str):
    """
    Class decorator used to add CODE to an Exception

    :param code: Exception error code
    :return: Decorated class
    """

    def decor(cls):
        cls.CODE = code
        cls.description = code.message
        if not isinstance(code, Code):
            raise ValueError(
                f'with_code requires valid error code string i.e "#10108", got: "{code}"'
            )
        cls.__name__ = f"e{code.raw_code}.{cls.__name__}"
        return cls

    return decor


@with_code(Sl1Codes.UNKNOWN)
class ApiException(HTTPException):
    code = 500

    def __init__(self, description=None, response=None):
        super(ApiException, self).__init__(description, response)
        self._is_json = False

    def create_link(self):
        return "https://help.prusa3d.com/{lang}/{code}{user_data}".format(
            lang=current_app.printer.locale,
            code=self.CODE.raw_code,
            user_data=current_app.printer.sys.help_page_url,
        )

    def to_html(self):
        error = self.get_translations()
        return create_error_page(
            title=error["title"],
            code=self.CODE.code,
            description=error["message"],
            link=self.create_link(),
        )

    def to_text(self) -> str:
        error = self.get_translations()
        return "{code}: {message}".format(code=self.CODE.code, message=error["message"])

    def to_json(self):
        error = self.get_translations()
        return json.dumps(
            {
                "title": error["title"],
                "code": self.CODE.code,
                "message": error["message"],
                "url": self.create_link(),
            }
        )

    def log(self):
        current_app.logger.error(
            "%s - %s - %s", self.CODE.code, self.CODE.title, self.description
        )

    def get_translations(self):
        return {
            "title": _("UNEXPECTED ERROR"),
            "message": _(
                "An unexpected error has occurred :-(.\n"
                "If the printer is printing, current job will be finished.\n"
                "You can turn the printer off by pressing the front power button.\n"
                "See the handbook to learn how to save a log file and send it to us."
            ),
        }

    def get_response(self, environ=None):
        headers = self.get_headers()
        headers.append(("Content-Location", f"{request.url_root}error"))
        accept = request.headers.get("Accept")

        body = ""
        if accept and "json" in accept:
            headers.append(("Content-Type", "application/json; charset=utf-8"))
            body = self.to_json()
        else:
            body = self.to_text()

        return Response(body, self.code, headers)


def generate_error_page():
    """render the last exception page or json

    Returns:
        (string, int): html, http status code
    """
    if State(current_app.printer.sys.state) == State.ERROR:
        exception_error = current_app.printer.last_error_or_warn
        return (exception_error.to_html(), 200)

    return (create_error_page(), 200)

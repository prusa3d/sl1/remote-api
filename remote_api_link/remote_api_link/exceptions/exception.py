# -*- coding: utf-8 -*-
# This file is part of the SLA firmware
# Copyright (C) 2021 Prusa Research a.s. - www.prusa3d.com
# SPDX-License-Identifier: GPL-3.0-or-later

import sys
import inspect

from prusaerrors.sl1.codes import Sl1Codes
from prusaerrors.shared.codes import Code

from .base import with_code, ApiException


@with_code(Sl1Codes.UNKNOWN)
class Unknown(ApiException):
    code = 500


@with_code(Sl1Codes.FILE_NOT_FOUND)
class FileNotFound(ApiException):
    code = 404

    def get_translations(self):
        return {
            "title": _("FILE NOT FOUND"),
            "message": _("Cannot find the selected file!"),
        }


@with_code(Sl1Codes.FILE_ALREADY_EXISTS)
class FileAlreadyExists(ApiException):
    code = 409

    def get_translations(self):
        return {
            "title": _("FILE ALREADY EXISTS"),
            "message": _(
                "File already exists! Delete it in the printer first and try again."
            ),
        }


@with_code(Sl1Codes.NOT_MECHANICALLY_CALIBRATED)
class NotMechanicallyCalibrated(ApiException):
    code = 409

    def get_translations(self):
        return {
            "title": _("CALIBRATION ERROR"),
            "message": _("The printer is not calibrated. Please run the Wizard first."),
        }


@with_code(Sl1Codes.NOT_UV_CALIBRATED)
class NotUvCalibrated(ApiException):
    code = 409

    def get_translations(self):
        return {
            "title": _("PRINTER NOT UV CALIBRATED"),
            "message": _(
                "The printer is not UV calibrated. Connect the UV calibrator and complete the calibration."
            ),
        }


@with_code(Sl1Codes.INVALID_PROJECT)
class InvalidProject(ApiException):
    code = 415

    def get_translations(self):
        return {
            "title": _("INVALID PROJECT"),
            "message": _("The project file is invalid!"),
        }


@with_code(Sl1Codes.NOT_AVAILABLE_IN_STATE)
class NotAvailableInState(ApiException):
    code = 409

    def get_translations(self):
        return {
            "title": _("PRINTER IS BUSY"),
            "message": _(
                "Cannot run this action, wait until the printer finishes the previous action."
            ),
        }


@with_code(Sl1Codes.NOT_ENOUGH_INTERNAL_SPACE)
class NotEnoughInternalSpace(ApiException):
    code = 409

    def get_translations(self):
        return {
            "title": _("INTERNAL MEMORY FULL"),
            "message": _(
                "Internal memory is full. Delete some of your projects first."
            ),
        }


@with_code(Sl1Codes.REMOTE_API_ERROR)
class RemoteApiError(ApiException):
    code = 400

    def get_translations(self):
        return {
            "title": _("REMOTE API ERROR"),
            "message": _(
                "This request is not compatible with Prusa remote API. See our documentation."
            ),
        }


@with_code(Sl1Codes.INVALID_PASSWORD)
class InvalidPassword(ApiException):
    code = 401

    def get_translations(self):
        return {
            "title": _("INVALID PASSWORD"),
            "message": _(
                "The password is incorrect. Please check or update it in:\n\n \
                Settings > Network > PrusaLink.\n\n \
                Prusa Slicer must use HTTP digest authorization type.\n \
                The password must be at least 8 characters long. Supported characters are letters, nubmes and dash."
            ),
        }


@with_code(Sl1Codes.PROJECT_ERROR_CANT_REMOVE)
class ProjectErrorCantRemove(ApiException):
    code = 409

    def get_translations(self):
        return {
            "title": _("CAN'T REMOVE PROJECT"),
            "message": _(
                "Remove it's not possible while there is a lock in the project file for print."
            ),
        }


@with_code(Sl1Codes.DIRECTORY_NOT_EMPTY)
class DirectoryNotEmpty(ApiException):
    code = 409

    def get_translations(self):
        return {
            "title": _("DIRECTORY NOT EMPTY"),
            "message": _("The directory is not empty."),
        }


@with_code(Sl1Codes.PROJECT_ERROR_CANT_READ)
class ProjectErrorCantRead(ApiException):
    code = 409

    def get_translations(self):
        return {
            "title": _("CAN'T READ PROJECT"),
            "message": _(
                "Opening the project failed, the file is possibly corrupted. Please re-slice or re-export the project, try again."  # noqa: E501
            ),
        }


@with_code(Sl1Codes.PROJECT_ERROR_NOT_ENOUGH_LAYERS)
class ProjectErrorNotEnoughLayers(ApiException):
    code = 409

    def get_translations(self):
        return {
            "title": _("NOT ENOUGHT LAYERS"),
            "message": _("The project must have at least one layer"),
        }


@with_code(Sl1Codes.PROJECT_ERROR_CORRUPTED)
class ProjectErrorCorrupted(ApiException):
    code = 409

    def get_translations(self):
        return {
            "title": _("PROJECT IS CORRUPTED"),
            "message": _(
                "Opening the project failed, the file corrupted. Please re-slice or re-export the project, try again."
            ),
        }


@with_code(Sl1Codes.PROJECT_ERROR_ANALYSIS_FAILED)
class ProjectErrorAnalysisFailed(ApiException):
    code = 409

    def get_translations(self):
        return {
            "title": _("PROJECT ANALYSIS FAILED"),
            "message": _("Analysis of the project has failed"),
        }


@with_code(Sl1Codes.PROJECT_ERROR_CALIBRATION_INVALID)
class ProjectErrorCalibrationInvalid(ApiException):
    code = 409

    def get_translations(self):
        return {
            "title": _("CALIBRATION PROJECT IS INVALID"),
            "message": _("Calibration project is invalid"),
        }


@with_code(Sl1Codes.PROJECT_ERROR_WRONG_PRINTER_MODEL)
class ProjectErrorWrongPrinterModel(ApiException):
    code = 409

    def get_translations(self):
        return {
            "title": _("WRONG PRINTER MODEL"),
            "message": _("The project is for different printer"),
        }


@with_code(Sl1Codes.PRINTING_DIRECTLY_WARNING)
class PrintingDirectlyFromMedia(ApiException):
    code = 409

    def get_translations(self):
        return {
            "title": _("CAN'T COPY PROJECT"),
            "message": _(
                "The internal memory is full, project cannot be copied. You can continue printing. However, you must not remove the USB drive during the print, otherwise the process will fail."  # noqa: E501
            ),
        }


@with_code(Sl1Codes.PROJECT_SETTINGS_MODIFIED_WARNING)
class ProjectSettingsModified(ApiException):
    code = 409

    def get_translations(self):
        return {
            "title": _("PARAMETERS OUT OF RANGE"),
            "message": _(
                "The print parameters are out of range of the printer, the system can try to fix the project. Proceed?"
            ),
        }


@with_code(Sl1Codes.PRINTER_VARIANT_MISMATCH_WARNING)
class VariantMismatch(ApiException):
    code = 409

    def __init__(self, last_exception):
        super(VariantMismatch, self).__init__()
        self.translation_data = {
            "project_variant": last_exception.get("project_variant", ""),
            "printer_variant": last_exception.get("printer_variant", ""),
        }

    def get_translations(self):
        return {
            "title": _("PRINTER VARIANT MISMATCH WARNING"),
            "message": _(
                "The model was sliced for a different printer variant %(project_variant)s. Your printer variant is %(printer_variant)s."  # noqa: E501
            )
            % self.translation_data,
        }


@with_code(Sl1Codes.TILT_HOME_FAILED)
class TiltFailed(ApiException):
    code = 409

    def get_translations(self):
        return {
            "title": _("TILT HOMING FAILED"),
            "message": _(
                "Tilt homing failed, check its surroundings and repeat the action."
            ),
        }


@with_code(Sl1Codes.TOWER_HOME_FAILED)
class TowerFailed(ApiException):
    code = 409

    def get_translations(self):
        return {
            "title": _("TOWER HOMING FAILED"),
            "message": _(
                "Tower homing failed, make sure there is no obstacle in its path and repeat the action."
            ),
        }


@with_code(Sl1Codes.TOWER_MOVE_FAILED)
class TowerMoveFailed(ApiException):
    code = 409

    def get_translations(self):
        return {
            "title": _("TOWER MOVING FAILED"),
            "message": _(
                "Moving the tower failed. Make sure there is no obstacle in its path and repeat the action."
            ),
        }


@with_code(Sl1Codes.TEMP_SENSOR_FAILED_ID)
class TempSensorFailed(ApiException):
    code = 409

    def get_translations(self):
        return {
            "title": _("TEMPERATURE SENSOR FAILED"),
            "message": _(
                "The temperature sensor failed. Check the wiring and connection."
            ),
        }


@with_code(Sl1Codes.FAN_FAILED_ID)
class FanFailed(ApiException):
    code = 409

    def __init__(self, last_exception):
        super(FanFailed, self).__init__()
        map_id = last_exception.get("fan__map_HardwareDeviceId", 0)
        self.text = "unknown fan"
        if map_id == 2000:
            self.text = "UV LED fan"
        elif map_id == 2001:
            self.text = "blower fan"
        elif map_id == 2002:
            self.text = "rear fan"

    def get_translations(self):
        return {
            "title": _("FAN FAILURE"),
            "message": _(
                "Incorrect RPM reading of the %(failed_fans_text)s fan. Please check its wiring and connection."
            )
            % {"failed_fans_text": self.text},
        }


@with_code(Sl1Codes.RESIN_SENSOR_FAILED)
class ResinFailed(ApiException):
    code = 409

    def get_translations(self):
        return {
            "title": _("RESIN SENSOR ERROR"),
            "message": _(
                "The resin sensor was not detected. Check the connection and amount of resin in the tank."
            ),
        }


@with_code(Sl1Codes.RESIN_TOO_LOW)
class ResinTooLow(ApiException):
    code = 409

    def __init__(self, last_exception):
        super(ResinTooLow, self).__init__()
        self.translation_data = {
            "volume_ml": last_exception.get("volume_ml", ""),
        }

    def get_translations(self):
        return {
            "title": _("RESIN TOO LOW"),
            "message": _(
                "Measured resin volume %(volume_ml)d ml is lower than required for this print. Refill the tank and restart the print."  # noqa: E501
            )
            % self.translation_data,
        }


@with_code(Sl1Codes.RESIN_TOO_HIGH)
class ResinTooHigh(ApiException):
    code = 409

    def __init__(self, last_exception):
        super(ResinTooHigh, self).__init__()
        self.translation_data = {
            "volume_ml": last_exception.get("volume_ml", ""),
        }

    def get_translations(self):
        return {
            "title": _("RESIN TOO HIGH"),
            "message": _(
                "Measured resin volume %(volume_ml)d ml is higher than required for this print. Make sure that the resin level does not exceed the 100%% mark and restart the print."  # noqa: E501
            )
            % self.translation_data,
        }


@with_code(Sl1Codes.WARNING_ESCALATION)
class WarningEscalation(ApiException):
    code = 409

    def get_translations(self):
        return {
            "title": _("PRINT JOB CANCELLED"),
            "message": _("The print job cancelled by the user."),
        }


@with_code(Sl1Codes.AMBIENT_TOO_HOT_WARNING)
class AmbientTooHot(ApiException):
    code = 409

    def get_translations(self):
        return {
            "title": _("AMBIENT TEMP. TOO HIGH"),
            "message": _(
                "The ambient temperature is too high, the print can continue, but it might fail."
            ),
        }


@with_code(Sl1Codes.AMBIENT_TOO_COLD_WARNING)
class AmbientTooCold(ApiException):
    code = 409

    def get_translations(self):
        return {
            "title": _("AMBIENT TEMP. TOO LOW"),
            "message": _(
                "The ambient temperature is too low, the print can continue, but it might fail."
            ),
        }


@with_code(Sl1Codes.RESIN_NOT_ENOUGH_WARNING)
class ResinNotEnough(ApiException):
    code = 409

    def get_translations(self):
        return {
            "title": _("NOT ENOUGH RESIN"),
            "message": _(
                "The amount of resin in the tank is not enough for the current project. Adding more resin will be required during the print."  # noqa: E501
            ),
        }


@with_code(Sl1Codes.RESIN_LOW)
class ResinLow(ApiException):
    code = 409

    def get_translations(self):
        return {
            "title": _("RESIN LOW"),
            "message": _(
                "Measured resin volume is low, the print can continue, but refill might be required."
            ),
        }


@with_code(Sl1Codes.INVALID_EXTENSION)
class InvalidExtension(ApiException):
    code = 415

    def get_translations(self):
        return {
            "title": _("INVALID FILE EXTENSION"),
            "message": _(
                "File has an invalid extension! See the article for supported file extensions."
            ),
        }


@with_code(Sl1Codes.UV_TEMP_SENSOR_FAILED)
class UvTempSensorFailed(ApiException):
    code = 409

    def get_translations(self):
        return {
            "title": _("UV LED TEMP. ERROR"),
            "message": _(
                "Reading of UV LED temperature has failed! This value is essential for the UV LED lifespan and printer safety. Please contact tech support! Current print job will be canceled."  # noqa: E501
            ),
        }


@with_code(Sl1Codes.FAN_WARNING)
class FanWarning(ApiException):
    code = 409

    def __init__(self, last_exception):
        super().__init__()
        self.translation_data = {
            "failed_fans_text": last_exception.get("failed_fans_text", ""),
        }

    def get_translations(self):
        return {
            "title": _("FAN WARNING"),
            "message": _(
                "Incorrect RPM reading of the %(failed_fans_text)s fan. Please check its wiring and connection."  # noqa: E501
            )
            % self.translation_data,
        }


@with_code(Sl1Codes.EXPECT_OVERHEATING)
class ExpectOverheating(ApiException):
    code = 409

    def __init__(self, last_exception):
        super().__init__()
        self.translation_data = {
            "failed_fans_text": last_exception.get("failed_fans_text", ""),
        }

    def get_translations(self):
        return {
            "title": _("EXPECT OVERHEATING"),
            "message": _(
                "Incorrect RPM reading of the %(failed_fans_text)s fan. Please check its wiring and connection. Expect overheating, but the print may continue."  # noqa: E501
            )
            % self.translation_data,
        }


@with_code(Sl1Codes.UNKNOWN_WARNING)
class UnknownWarning(ApiException):
    code = 409

    def get_translations(self):
        return {
            "title": _("UNKNOWN WARNING"),
            "message": _(
                "An unknown warning has occured. Restart the printer and try again.\n"
                "Contact our tech support if the problem persists."
            ),
        }


def create_map_code_class():
    clsmembers = inspect.getmembers(sys.modules[__name__], inspect.isclass)

    result = {}
    for _, cls in clsmembers:
        if issubclass(cls, ApiException):
            result[cls.CODE.code] = (cls, "__init__" in cls.__dict__)

    return result


CODE_ERROR = create_map_code_class()


def get_error_by_code(last_exception):
    code = last_exception["code"]
    if code in [Sl1Codes.NONE.code, Sl1Codes.NONE_WARNING.code]:
        return None

    error_map = CODE_ERROR.get(code)
    if error_map:
        cls, has_init = error_map
        if has_init:
            return cls(last_exception)
        return cls()

    raise Unknown(f"Unknown error code {code} in remote_api_link")


def dbus_exception_to_error(exception: Exception) -> Code:
    code = exception.message.removeprefix("GDBus.Error:e").split(".")[0]
    if code:
        out_dict = {"code": "#" + code}
        return get_error_by_code(out_dict)

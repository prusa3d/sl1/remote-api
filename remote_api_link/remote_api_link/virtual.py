import logging
from unittest.mock import patch, Mock

from remote_api_link.__main__ import main

logging.basicConfig(
    format="%(asctime)s - %(levelname)s - %(name)s - %(message)s", level=logging.DEBUG
)

# log into current terminal instead of journalctl
with patch("remote_api_link.factory.configure_log", Mock):
    main()

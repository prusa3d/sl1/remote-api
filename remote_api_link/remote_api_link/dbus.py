# -*- coding: utf-8 -*-
# This file is part of the SLA firmware
# Copyright (C) 2021 Prusa Research a.s. - www.prusa3d.com
# SPDX-License-Identifier: GPL-3.0-or-later

import logging
from defusedxml.ElementTree import fromstring

from gi.repository.GLib import GError


def create_method(name):
    def wrapper(self, *args, **kwds):
        try:
            return getattr(self.bus_obj, name)(*args, **kwds)
        except GError as error:
            self._logger.error(
                "Exception happend when call [%s].%s\n%s",
                self.__class__.__name__,
                name,
                error,
            )
            raise error

    wrapper.__name__ = name
    return wrapper


def create_property(name):
    def get_wrapper(self, *args, **kwds):
        try:
            return getattr(self.bus_obj, name)
        except GError as error:
            self._logger.error(
                "Exception happend when call [%s].%s\n%s",
                self.__class__.__name__,
                name,
                error,
            )
            raise error

    def set_wrapper(self, value):
        try:
            return setattr(self.bus_obj, name, value)
        except GError as error:
            self._logger.error(
                "Exception happend when call [%s].%s\n%s",
                self.__class__.__name__,
                name,
                error,
            )
            raise error

    return property(get_wrapper, set_wrapper)


def constructor(self, bus_obj):
    self.bus_obj = bus_obj
    self._logger = logging.getLogger()


def create_bus(bus, interface_name):
    bus_obj = bus.get(interface_name)
    root = fromstring(bus_obj.Introspect())
    interface = root.findall(f"./interface[@name='{interface_name}']/*")

    members = {}
    for child in interface:
        func_name = child.attrib["name"]
        if child.tag == "property":
            members[func_name] = create_property(func_name)
        else:
            members[func_name] = create_method(func_name)

    members["__init__"] = constructor
    return type(interface_name, (object,), members)(bus_obj)

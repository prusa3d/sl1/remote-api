# -*- coding: utf-8 -*-
# This file is part of the SLA firmware
# Copyright (C) 2021 Prusa Research a.s. - www.prusa3d.com
# SPDX-License-Identifier: GPL-3.0-or-later

from flask import current_app

from .. import api


@api.route("/v1/info", methods=["GET"])
def info():
    sys_info = current_app.printer.sys.info
    name = sys_info["name"]
    serial = sys_info["sn"]

    return {
        "name": name,
        "location": "",
        "hostname": current_app.printer.sys.net_hostname,
        "serial": serial,
        "project_extensions": current_app.printer.project_extensions,
    }

# -*- coding: utf-8 -*-
# This file is part of the SLA firmware
# Copyright (C) 2021 Prusa Research a.s. - www.prusa3d.com
# SPDX-License-Identifier: GPL-3.0-or-later

import os
from time import time
from zipfile import BadZipFile

from flask import current_app, request, url_for
from gi.repository.GLib import GError
from prusaerrors.sl1.codes import Sl1Codes

from .. import api

from remote_api_link import defines
from remote_api_link.exceptions import (
    RemoteApiError,
    ProjectErrorCorrupted,
    ProjectErrorCantRead,
    FileNotFound,
    dbus_exception_to_error,
)
from remote_api_link.states import State


def map_type(filetype, filename):
    printer = current_app.printer
    if filetype == "folder":
        return "FOLDER"
    (_, extension) = os.path.splitext(filename)

    if extension in printer.project_extensions:
        return "PRINT_FILE"

    if extension == "xz":
        return "FIRMWARE"

    return "FILE"


def map_file_info(storage_path, file_info, last_update=None, map_folder_children=False):
    origin = file_info["origin"]
    path = file_info["path"]
    rel_path = os.path.relpath(path, start=storage_path)
    virtual_path = os.path.abspath(os.path.join("/", origin, rel_path))
    name = os.path.basename(path)

    result = {
        "name": name,
        "path": os.path.dirname(virtual_path),
        "read_only": False,
        "m_timestamp": file_info["mtime"] if file_info["mtime"] != 0 else last_update,
        "type": map_type(file_info["type"], name),
    }
    if "size" in file_info:
        result["size"] = file_info["size"]

    if map_folder_children and "children" in file_info:
        result["children"] = [
            map_file_info(storage_path, i, last_update) for i in file_info["children"]
        ]

    if result["type"] == "PRINT_FILE":
        result["refs"] = {
            "download": url_for(
                ".downloads", target=origin, filename=rel_path, _external=False
            ),
        }
        if "metadata" not in file_info:
            get_metadata = current_app.printer.fs.get_metadata(path, 1)
            if "metadata" not in get_metadata["files"]:
                return result
            file_info["metadata"] = get_metadata["files"]["metadata"]

        # parse the metadata
        config = file_info["metadata"].get("config", {})
        metadata = {}
        if "printTime" in config:
            metadata["estimated_print_time"] = config["printTime"]
        if "layer_height" in config:
            metadata["layer_height"] = config["layerHeight"]
        if "materialName" in config:
            if material := config["materialName"]:
                metadata["material"] = material.split("@")[0].strip()
        if "expTime" in config:
            metadata["exposure_time"] = config["expTime"]
        if "expTimeFirst" in config:
            metadata["exposure_time_first"] = config["expTimeFirst"]
        if "expTimeCalibrate" in config:
            metadata["exposure_time_calibration"] = config["expTimeCalibrate"]
        if "exposureUserProfile" in config:
            metadata["exposure_user_profile"] = config["exposureUserProfile"]
        if "layers" in config:
            metadata["layers"] = config["layers"]
        result["meta"] = metadata
        if "thumbnail" in file_info["metadata"]:
            thumbnail = file_info["metadata"]["thumbnail"]
            if "files" in thumbnail:
                for path in thumbnail["files"]:
                    link = os.path.relpath(path, start=defines.TMP_FOLDER)
                    if path[-11:-4] == "400x400":
                        result["refs"]["icon"] = url_for(
                            ".thumbnails",
                            filename=link,
                            _external=False,
                        )
                    else:
                        result["refs"]["thumbnail"] = url_for(
                            ".thumbnails",
                            filename=link,
                            _external=False,
                        )
    return result


@api.route("/v1/files/<string:target>/", methods=["GET"], defaults={"filename": None})
@api.route("/v1/files/<string:target>/<path:filename>", methods=["GET"])
def get_files_by_origin_and_path(target, filename):
    printer = current_app.printer
    if printer.is_not_modified(request.headers):
        return defines.NOT_MODIFIED

    try:
        storage_path = printer.convert_path(target, "")
    except FileNotFound:
        return defines.NOT_FOUND_JSON

    path = storage_path if not filename else printer.convert_path(target, filename)
    maxdepth = 2
    data_source = printer.fs.get_from_path(path, maxdepth)  # check if exist

    if not data_source["files"]["exists"]:
        return defines.NOT_FOUND_JSON

    payload = map_file_info(
        storage_path,
        data_source["files"],
        data_source["last_update"][target],
        map_folder_children=True,
    )

    return printer.make_response(payload)


@api.route("/v1/files/<string:target>/<path:filename>", methods=["POST"])
def post_project_file(target, filename):
    printer = current_app.printer
    if target not in ["local", "usb"]:
        raise RemoteApiError()

    if State(printer.sys.state) == State.SELECTED:
        printer.sys.cmd_cancel()

    auto_advance = True

    path = printer.convert_path(target, filename)  # convert path
    printer.fs.get_from_path(path, 0)  # check if exist

    ignore_errors = False
    printer.sys.cmd_select(path, auto_advance, ignore_errors)

    return defines.NO_CONTENT_JSON


@api.route("/v1/files/<string:target>/<path:filename>", methods=["DELETE"])
def delete_project_file(target, filename):
    printer = current_app.printer
    path = printer.convert_path(target, filename)  # convert path
    printer.sys.cmd_try_cancel_by_path(path)
    printer.fs.remove(path)

    return defines.NO_CONTENT_JSON


@api.route("/v1/files/<string:target>/<path:filename>", methods=["PUT"])
def put_project_file(target, filename):
    # TODO: this duplicates "Download manager" in FM.
    # Main problem is how to pass handle to stream over DBus
    printer = current_app.printer

    headers = request.headers
    content_length = int(headers.get("content-length"))
    overwrite = headers.get("overwrite") == "?1"
    create_folder = headers.get("create-folder") == "?1"

    upload_name = os.path.basename(filename)
    upload_path = os.path.dirname(filename)

    if create_folder:
        internal_path = printer.convert_path(target, upload_path)
        printer.fs.create_folder(internal_path + "/" + upload_name)
        return defines.CREATED_JSON

    try:
        file_path = printer.fs.validate_project(
            upload_name, target, upload_path, content_length, overwrite
        )
    except GError as e:
        return dbus_exception_to_error(e)

    # create touch-ui notification
    upload_handle: int = printer.upload_number(
        upload_name, 0, content_length, file_path
    )

    printer.fs.start_transfer(
        target,
        file_path,
        content_length,
        "FROM_WEB",
        True,
        0,
    )

    try:
        timer = time()
        with open(file_path, "wb") as f:
            while True:
                chunk = request.stream.read(2048)
                if not chunk:
                    break
                f.write(chunk)
                if timer + 0.2 < time():
                    printer.fs.update_transfer_progress(len(chunk))
                    # update touch-ui notification
                    printer.upload_modify(
                        upload_handle,
                        "progress",
                        printer.fs.transfer["progress"] / 100,
                    )
                    timer = time()
    except BadZipFile:
        printer.upload_error(upload_handle, Sl1Codes.PROJECT_ERROR_CORRUPTED)
        raise ProjectErrorCorrupted()
    except IOError:
        printer.upload_error(upload_handle, Sl1Codes.PROJECT_ERROR_CANT_READ)
        raise ProjectErrorCantRead()

    printer.fs.stop_transfer()
    printer.upload_modify(upload_handle, "progress", 1)

    return defines.CREATED_JSON

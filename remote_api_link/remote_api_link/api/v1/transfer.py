import logging
import os
from flask import current_app

from .. import api


def get_transfer_info(full=False):
    printer = current_app.printer
    transfer = printer.fs.transfer
    transfer_id = transfer.get("id")

    logger = logging.getLogger(__name__)
    if not transfer_id:
        return None

    if transfer["progress"] == 1.0 and not full:
        return None

    logger.info(f"transfer {transfer}")

    result = {
        "id": transfer_id,
        "time_transferring": transfer["time_elapsed"],
        "progress": transfer["progress"],
        "data_transferred": transfer["size_transferred"],
    }

    name = os.path.basename(transfer["path"])

    result["name"] = name
    result["display_name"] = name
    result["size"] = transfer["size_total"]
    result["path"] = transfer["path"]
    result["to_print"] = transfer["to_print"]

    return result


@api.route("/v1/transfer", methods=["GET"])
def transfer():
    return get_transfer_info(full=True)

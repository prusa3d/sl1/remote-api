# -*- coding: utf-8 -*-
# This file is part of the SLA firmware
# Copyright (C) 2021 Prusa Research a.s. - www.prusa3d.com
# SPDX-License-Identifier: GPL-3.0-or-later

import os
from flask import current_app
from enum import Enum, unique
from werkzeug.routing import BaseConverter, ValidationError

from .. import api
from .files import map_file_info

from ...states import State

from remote_api_link import defines
from gi.repository.GLib import GError


@unique
class JobCommandType(str, Enum):
    Pause = "pause"
    Resume = "resume"
    # Continue = 'continue'


class JobCommandTypeConverter(BaseConverter):
    def to_python(self, value):
        try:
            request_type = JobCommandType(value)
            return request_type
        except ValueError as err:
            raise ValidationError from err

    def to_url(self, obj):
        return obj.value


def get_job_info(full=False):
    printer = current_app.printer
    state = State(printer.sys.state)

    if state in [State.READY, State.ERROR, State.FINISHED, State.STOPPED]:
        return None

    try:
        job = printer.sys.job

        result = {
            "id": job["instance_id"],  # integer
            "progress": round(job["progress"] * 100),  # percentage
            "time_remaining": job["remaining_time"],  # seconds
            "time_printing": job["time_elapsed"],  # seconds
            # job print settings
            "exposure_time": job["exposureTime"],
            "exposure_time_first": job["exposureTimeFirst"],
            "current_layer": job["current_layer"],
            "resin_consumed": job["consumed_material"],
            "resin_low": job["resin_low"],
        }

        if "exposureTimeCalibration" in job:
            result["exposure_time_calibration"] = job["exposureTimeCalibration"]

        if job["remaining_material"] != -1:
            result["resin_remaining"] = job["remaining_material"]

        if full:
            result["state_text"] = state.get_flags()["text"]
            path = os.path.realpath(job["path"])
            data_source = printer.fs.get_metadata(path, False)
            data_source = data_source["files"]
            origin = data_source["origin"]
            storage_path = printer.convert_path(origin)
            result["file"] = map_file_info(storage_path, data_source)

        return result
    except GError:
        return None


@api.route("/v1/job", methods=["GET"])
def job():
    return get_job_info(full=True)


@api.route("/v1/job/<int:job_id>/<job_command_type:command>", methods=["PUT"])
def put_job_pause(job_id, command):
    printer = current_app.printer
    state = State(printer.sys.state)

    if state in [State.READY, State.ERROR]:
        return None

    job = printer.sys.job

    if job["instance_id"] != job_id:
        return defines.NOT_FOUND_JSON

    printer = current_app.printer

    if command == JobCommandType.Pause:
        printer.sys.cmd_pause()
    elif command == JobCommandType.Resume:
        printer.sys.cmd_continue()
    else:
        return defines.BAD_REQUEST_JSON

    return defines.NO_CONTENT_JSON


@api.route("/v1/job/<int:job_id>", methods=["DELETE"])
def delete_job(job_id):
    printer = current_app.printer
    state = State(printer.sys.state)

    if state in [State.READY, State.ERROR]:
        return None

    job = printer.sys.job

    if job["instance_id"] != job_id:
        return defines.NOT_FOUND_JSON

    printer = current_app.printer

    printer.sys.cmd_cancel()

    return defines.NO_CONTENT_JSON

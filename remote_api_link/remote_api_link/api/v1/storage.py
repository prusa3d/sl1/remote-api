# -*- coding: utf-8 -*-
# This file is part of the SLA firmware
# Copyright (C) 2021 Prusa Research a.s. - www.prusa3d.com
# SPDX-License-Identifier: GPL-3.0-or-later

from flask import current_app

from .. import api


def get_storage_list():
    filemanager = current_app.printer.fs
    disk_usage = filemanager.disk_usage
    usb_name = filemanager.usb_name

    return [
        {
            "path": "/local",
            "read_only": False,
            "free_space": disk_usage["local"]["free"],
            "name": "Local",
            "type": "LOCAL",
            "available": True,
        },
        {
            "path": "/usb",
            "read_only": False,
            # "free_space": disk_usage["usb"]["free"],
            "name": "USB",
            "type": "USB",
            "available": usb_name != "",
        },
    ]


@api.route("/v1/storage", methods=["GET"])
def storage():
    return {"storage_list": get_storage_list()}

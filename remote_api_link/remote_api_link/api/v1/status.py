# -*- coding: utf-8 -*-
# This file is part of the SLA firmware
# Copyright (C) 2021 Prusa Research a.s. - www.prusa3d.com
# SPDX-License-Identifier: GPL-3.0-or-later

from flask import current_app

from .. import api

from .storage import get_storage_list
from .job import get_job_info
from .transfer import get_transfer_info

from ...states import State


@api.route("/v1/status", methods=["GET"])
def status():
    storage_list = get_storage_list()

    result = {
        "printer": get_printer_info(),
        "storage": {i: storage_list[i] for i in range(len(storage_list))},
    }

    camera_info = get_camera_info()
    if camera_info:
        result["camera"] = camera_info

    transfer_info = get_transfer_info()
    if transfer_info:
        result["transfer"] = transfer_info

    job_info = get_job_info()
    if job_info:
        result["job"] = job_info

    return result


def get_printer_info():
    printer = current_app.printer
    connect = printer.connect
    telemetry = printer.sys.hw_telemetry
    state = State(printer.sys.state)
    temperatures = telemetry["temperatures"]
    fans = telemetry["fans"]
    connect_message = "OK" if connect.is_registered else "Not registered"

    if not connect.is_ok:
        connect_message = "Communication error"

    status_printer = {"ok": True, "message": "OK"}
    if state == State.ERROR and printer.last_error_or_warn:
        status_printer = {"ok": False, "message": printer.last_error_or_warn.to_text()}
    return {
        "state": state.get_text(),
        "temp_uv_led": temperatures["temp_led"],
        "temp_cpu": temperatures["cpu_temp"],
        "temp_ambient": temperatures["temp_amb"],
        "fan_uv_led": fans["uv_led"],
        "fan_blower": fans["blower"],
        "fan_rear": fans["rear"],
        "cover_closed": telemetry["cover_closed"],
        "is_calibrated": telemetry["is_calibrated"],
        "status_connect": {
            "ok": connect.is_ok,
            "message": connect_message,
        },
        "status_printer": status_printer,
    }


def get_camera_info():
    return None

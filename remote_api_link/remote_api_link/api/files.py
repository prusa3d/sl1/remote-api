# -*- coding: utf-8 -*-
# This file is part of the SLA firmware
# Copyright (C) 2021 Prusa Research a.s. - www.prusa3d.com
# SPDX-License-Identifier: GPL-3.0-or-later

import os
import zipfile
import tempfile
import threading

from flask import current_app, request, url_for
from streaming_form_data import StreamingFormDataParser
from streaming_form_data.targets import FileTarget, ValueTarget

from prusaerrors.sl1.codes import Sl1Codes

from . import api

from remote_api_link import defines

from remote_api_link.exceptions import (
    RemoteApiError,
    ProjectErrorCorrupted,
    ProjectErrorCantRead,
)
from remote_api_link.utils import valid_boolean_trues
from remote_api_link.states import State


def convert(parent_path, origin, children, fs):
    if not children:
        return []

    for child in children:
        child["name"] = os.path.basename(child["path"])
        child["display"] = child["name"]
        internal_path = child["path"]
        child["path"] = os.path.relpath(internal_path, start=parent_path)
        child["origin"] = origin
        child["date"] = child["mtime"]
        del child["mtime"]
        if child["type"] == "folder":
            child["typePath"] = ["folder"]
            convert(parent_path, origin, child.get("children"), fs)
        else:
            child["type"] = "machinecode"
            child["typePath"] = ["machinecode", "gcode"]
            refs = {
                "resource": url_for(
                    ".get_project",
                    target=origin,
                    filename=child["path"],
                    _external=True,
                ),
                "download": url_for(
                    ".downloads", target=origin, filename=child["path"], _external=True
                ),
            }
            if "metadata" in child:
                analysis = {}
                metadata = child["metadata"]
                config = metadata.get("config", {})
                del child["metadata"]
                if "printTime" in config:
                    analysis["estimatedPrintTime"] = config["printTime"]
                if "layerHeight" in config:
                    analysis["layerHeight"] = config["layerHeight"]
                if "materialName" in config:
                    if material := config["materialName"]:
                        analysis["material"] = material.split("@")[0].strip()
                # SL1 metadata
                if "exposureTime" in metadata:
                    analysis["exposureTime"] = metadata["exposureTime"]
                if "exposureTimeFirst" in metadata:
                    analysis["exposureTimeFirst"] = metadata["exposureTimeFirst"]
                if "exposureUserProfile" in metadata:
                    analysis["exposureUserProfile"] = metadata["exposureUserProfile"]
                if "layers" in metadata:
                    analysis["layers"] = metadata["layers"]
                ##
                if analysis:
                    child["gcodeAnalysis"] = analysis
                if "thumbnail" in metadata and "files" in metadata["thumbnail"]:
                    thumbnail = metadata["thumbnail"]
                    for path in thumbnail["files"]:
                        link = os.path.relpath(path, start=defines.TMP_FOLDER)
                        if path[-11:-4] == "400x400":
                            refs["thumbnailSmall"] = url_for(
                                ".thumbnails",
                                filename=link,
                                _external=True,
                            )
                        else:
                            refs["thumbnailBig"] = url_for(
                                ".thumbnails",
                                filename=link,
                                _external=True,
                            )
            child["refs"] = refs

    return children


@api.route("/files", methods=["GET"])
def get_all_projects():
    if current_app.printer.is_not_modified(request.headers):
        return defines.NOT_MODIFIED

    recursive = valid_boolean_trues(request.args.get("recursive", False))
    maxdepth = -1 if recursive else 1
    fs = current_app.printer.fs

    current_app.printer.load_files(depth=maxdepth)
    data = current_app.printer.files
    result = []
    for source in data:
        if source in ["usb", "local"]:
            data_source = data[source]
            origin = "local" if data_source["origin"] == "local" else "usb"
            result.extend(
                convert(data_source["path"], origin, data_source.get("children"), fs)
            )
    disk_usage = fs.disk_usage
    free = disk_usage["local"]["free"]
    total = disk_usage["local"]["total"]
    response = current_app.printer.make_response(
        {"files": result, "free": free, "total": total}
    )
    return response


@api.route("/files/<string:target>", methods=["GET"])
def get_projects_by_origin(target):
    printer = current_app.printer
    if printer.is_not_modified(request.headers):
        return defines.NOT_MODIFIED

    recursive = valid_boolean_trues(request.args.get("recursive", False))
    maxdepth = -1 if recursive else 1

    path = printer.convert_path(target)  # convert path
    data_source = printer.fs.get_from_path(path, maxdepth)  # check if exist
    result = []
    if "files" in data_source:
        data_source = data_source["files"]
        result = convert(
            data_source.get("path"), target, data_source.get("children"), printer.fs
        )

    disk_usage = printer.fs.disk_usage
    free = disk_usage["local"]["free"]
    total = disk_usage["local"]["total"]
    return printer.make_response({"files": result, "free": free, "total": total})


@api.route("/files/<string:target>/<path:filename>", methods=["GET"])
def get_project(target, filename):
    if current_app.printer.is_not_modified(request.headers):
        return defines.NOT_MODIFIED
    printer = current_app.printer
    other_sources = []
    if State(printer.sys.state) == State.PRINTING:
        other_sources = ["previous-prints"]
    path = printer.convert_path(target, filename, permite_other_sources=other_sources)
    data_source = printer.fs.get_metadata(path, True)
    result = convert(
        printer.convert_path(target, permite_other_sources=other_sources),
        target,
        [data_source["files"]],
        printer.fs,
    )[0]
    return printer.make_response(result)


@api.route("/files/<string:target>", methods=["POST"])
def upload_project(target):
    printer = current_app.printer
    upload_handle = -1

    headers = request.headers
    content_length = None
    for key in headers.keys():
        if key.lower() == "content-length":
            content_length = headers.get(key)
            break

    if content_length:
        content_length = int(content_length)

    with tempfile.TemporaryDirectory() as tmpdirname:
        path_ = ValueTarget()
        select_ = ValueTarget()
        tmp_file = os.path.join(tmpdirname, "file.dat")
        file_ = FileTarget(tmp_file)
        parser = StreamingFormDataParser(headers=headers)
        parser.register("path", path_)
        parser.register("file", file_)
        parser.register("select", select_)

        length = 0
        file_path = None

        progress_alive = threading.Event()
        progress_alive.clear()

        def progress_thread(progress_alive):
            nonlocal upload_handle
            progress_alive.wait()

            while progress_alive.is_set():
                printer.upload_modify(
                    upload_handle,
                    "progress",
                    length / content_length,
                )

        pt = threading.Thread(
            target=progress_thread, args=(progress_alive,), daemon=True
        )
        try:
            while True:
                chunk = request.stream.read(5000000)
                if not chunk:
                    break
                length += len(chunk)

                parser.data_received(chunk)
                if not file_path:
                    name = file_.multipart_filename
                    if name:
                        path = ""
                        if path_._finished:
                            path = path_.value.decode("utf-8")
                        file_path, upload_handle = printer.fs.validate_project(
                            name, target, path, content_length, True
                        )

                        progress_alive.set()
                        pt.start()

            if file_path is None:
                raise FileNotFoundError()

        except zipfile.BadZipFile:
            printer.upload_error(upload_handle, Sl1Codes.PROJECT_ERROR_CORRUPTED)
            raise ProjectErrorCorrupted()
        except IOError:
            printer.upload_error(upload_handle, Sl1Codes.PROJECT_ERROR_CANT_READ)
            raise ProjectErrorCantRead()
        finally:
            progress_alive.clear()
        printer.fs.move(tmp_file, file_path)
        printer.upload_modify(upload_handle, "progress", 1)

    data_source = printer.fs.get_metadata(file_path, True)
    result = convert(
        printer.convert_path(target), target, [data_source["files"]], printer.fs
    )[0]
    printer.load_files(depth=-1)
    return result, 201


@api.route("/files/<string:target>/<path:filename>", methods=["POST"])
def command_project(target, filename):
    printer = current_app.printer
    if target not in ["local", "usb"]:
        raise RemoteApiError()

    data = request.json
    if data is None:
        raise RemoteApiError()

    command = data.get("command")
    if command not in ["select"]:
        raise RemoteApiError()

    if State(printer.sys.state) == State.SELECTED:
        printer.sys.cmd_cancel()

    auto_advance: bool = False
    if "print" in data and valid_boolean_trues(data["print"]):
        auto_advance = True

    path = printer.convert_path(target, filename)  # convert path
    printer.fs.get_from_path(path, 0)  # check if exist

    ignore_errors = False
    printer.sys.cmd_select(path, auto_advance, ignore_errors)

    return defines.NO_CONTENT


@api.route("/files/<string:target>/<path:filename>", methods=["DELETE"])
def delete_project(target, filename):
    printer = current_app.printer
    path = printer.convert_path(target, filename)  # convert path
    printer.sys.cmd_try_cancel_by_path(path)
    printer.fs.remove(path)

    return defines.NO_CONTENT

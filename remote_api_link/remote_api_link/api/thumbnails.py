# -*- coding: utf-8 -*-
# This file is part of the SLA firmware
# Copyright (C) 2021 Prusa Research a.s. - www.prusa3d.com
# SPDX-License-Identifier: GPL-3.0-or-later

import os

from flask import send_file

from . import api

from remote_api_link import defines
from remote_api_link.exceptions import FileNotFound


@api.route("/thumbnails/<path:filename>", methods=["GET"])
def thumbnails(filename):
    """Retrieve a specific file’s thumbnails."""

    thumbnail = None
    thumbnail = os.path.abspath(os.path.join(defines.TMP_FOLDER, filename))
    if os.path.isfile(thumbnail) and thumbnail.startswith(defines.TMP_FOLDER):
        return send_file(
            thumbnail,
            attachment_filename=os.path.basename(thumbnail),
        )

    raise FileNotFound()

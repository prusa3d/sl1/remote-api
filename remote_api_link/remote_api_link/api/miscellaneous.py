# -*- coding: utf-8 -*-
# This file is part of the SLA firmware
# Copyright (C) 2021 Prusa Research a.s. - www.prusa3d.com
# SPDX-License-Identifier: GPL-3.0-or-later

from flask import current_app, jsonify, request, url_for

from . import api

from remote_api_link import defines, __version__
from remote_api_link.states import State
from prusa.connect.printer import __version__ as sdk_version


@api.route("/version", methods=["GET"])
def version():
    sys_info = current_app.printer.sys.info
    name = sys_info["name"]
    firmware = sys_info["firmware"]

    return jsonify(
        {
            "api": "0.9.0-legacy",  # aligned with PrusaLink
            "version": __version__,
            "firmware": firmware,
            "printer": name,
            "sdk": sdk_version,
            "text": f"Prusa SLA {firmware}",
            "capabilities": {
                "upload-by-put": True,
            },
        }
    )


@api.route("/connection", methods=["GET", "POST", "DELETE"])
def connection():
    connect = current_app.printer.connect
    if request.method == "POST":
        data = request.json
        connect_data = data.get("connect")
        if connect:
            hostname = connect_data["hostname"]
            port = connect_data["port"]
            tls = connect_data["tls"]

            connect.set_server(hostname, port, tls)
            url = connect.register_printer()

            return jsonify({"url": url})

        return ("", 204)
    elif request.method == "DELETE":
        connect.unregister_printer()
        return ("", 204)
    else:
        api_state = State(current_app.printer.sys.state).get_flags()
        connect_settings = connect.settings
        connect_registration = connect.registration_status
        return jsonify(
            {
                "connect": {
                    "hostname": connect_settings["hostname"],
                    "port": connect_settings["port"],
                    "tls": connect_settings["tls"],
                    "registration": connect_registration,
                },
                "current": {
                    "baudrate": 115200,
                    "port": "VIRTUAL",
                    "printerProfile": "_default",
                    "state": api_state["text"],
                },
                "options": {
                    "baudratePreference": 115200,
                    "baudrates": [115200],
                    "portPreference": "VIRTUAL",
                    "ports": ["VIRTUAL"],
                    "printerProfilePreference": "_default",
                    "printerProfiles": [
                        {"id": "_default", "name": "Default"},
                    ],
                },
            }
        )


def get_defalt_profile():
    project_extensions = current_app.printer.project_extensions
    return {
        "id": "_default",
        "name": "Default",
        "model": "Original Prusa SLA",
        "color": "default",
        "current": True,
        "default": True,
        "resource": url_for(".get_printer_profile", _external=True),
        "heatedBed": True,
        "heatedChamber": True,
        "projectExtensions": project_extensions,
        "extruder": {
            "count": 1,
            "offsets": [0.0, 0.0],
        },
    }


@api.route("/printerprofiles", methods=["GET"])
def printer_profiles():
    return jsonify(
        {
            "profiles": [get_defalt_profile()],
        }
    )


@api.route("/printerprofiles/_default", methods=["GET"])
def get_printer_profile():
    return jsonify(get_defalt_profile())


@api.route("/access/users", methods=["GET"])
def access_users():
    return jsonify(
        {
            "users": [
                {
                    "active": True,
                    "apikey": None,
                    "name": "maker",
                    "settings": {},
                    "user": True,
                    "admin": False,
                },
            ],
        }
    )


@api.route("/login", methods=["GET", "POST"])
def login():
    return {
        "session": "dummy",
        "_is_external_client": True,
    }


@api.route("/logout", methods=["POST"])
def logout():
    return defines.NO_CONTENT


@api.route("/currentuser", methods=["GET"])
def get_current_user():
    return {
        "name": "Prusa",
    }


@api.route("/timelapse", methods=["GET", "POST"])
def timelapse():
    return jsonify({"config": {"type": "off"}, "enabled": False, "files": []})


@api.route("/settings", methods=["GET", "POST"])
def settings():
    return {"appearance": {"color": "black"}}

# -*- coding: utf-8 -*-
# This file is part of the SLA firmware
# Copyright (C) 2021 Prusa Research a.s. - www.prusa3d.com
# SPDX-License-Identifier: GPL-3.0-or-later

import os
from flask import current_app, send_file, request
from flask.json import jsonify
from gi.repository.GLib import GError


from . import api
from remote_api_link.exceptions import RemoteApiError, dbus_exception_to_error


@api.route("/downloads/<string:target>/<path:filename>", methods=["GET"])
def downloads(target, filename):
    printer = current_app.printer
    path = printer.convert_path(target, filename)  # convert path
    printer.fs.get_from_path(path, 0)  # check if exist
    return send_file(
        path,
        attachment_filename=os.path.basename(path),
    )


@api.route("/download", methods=["GET"])
def get_download_file_progress():
    transfer = current_app.printer.fs.transfer
    if transfer:
        return jsonify(transfer), 200
    return "", 204


@api.route("/download/<string:target>", methods=["POST"])
def download_file_remote_url(target):
    data = request.json
    printer = current_app.printer
    if None in (data["url"], data["destination"]):
        return RemoteApiError()
    file_name = data["url"].split("/")[-1]
    if data["rename"]:
        file_name = data["rename"]
        if not os.path.splitext(file_name)[1]:
            file_name += os.path.splitext(data["url"])[1]
    destination = data["destination"] + "/" if data["destination"] else ""
    try:
        printer.fs.download_from_url(
            data["url"], target, destination + file_name, "FROM_WUI", True, None, 0
        )
    except GError as e:
        return dbus_exception_to_error(e)
    return "", 201

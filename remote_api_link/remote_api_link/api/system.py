# -*- coding: utf-8 -*-
# This file is part of the SLA firmware
# Copyright (C) 2021 Prusa Research a.s. - www.prusa3d.com
# SPDX-License-Identifier: GPL-3.0-or-later

from pydbus import Variant
from flask import current_app, request, url_for, jsonify

from . import api

from remote_api_link import defines
from remote_api_link.exceptions import RemoteApiError


def get_commands():
    commands = []
    for command in [
        ("resinrefill", "Refill resin"),
        ("resinrefilled", "Resin fully refilled"),
        ("changeexposure", "Change exposure times"),
    ]:
        commands.append(
            {
                "action": command[0],
                "name": command[1],
                "source": "custom",
                "resource": url_for(
                    f".execute_command_{command[0]}",
                    _external=True,
                ),
            }
        )
    return commands


@api.route("/system/commands", methods=["GET"])
def system_commands():
    """List all registered system commands"""
    return jsonify(
        {
            "core": [],
            "custom": get_commands(),
        }
    )


@api.route("/system/commands/<string:source>", methods=["GET"])
def system_commands_by_source(source):
    """List all registered system commands for a source"""
    if source == "core":
        return jsonify([])
    if source == "custom":
        return jsonify(get_commands())
    raise RemoteApiError()


@api.route("/system/commands/custom/resinrefill", methods=["POST"])
def execute_command_resinrefill():
    """Execute the registered system command resinrefill"""
    current_app.printer.sys.cmd_resin_refill()
    return defines.NO_CONTENT


@api.route("/system/commands/custom/changeexposure", methods=["POST"])
def execute_command_changeexposure():
    """Execute the registered system command changeexposure"""
    convert = {
        "exposureTime": "exposure_time_ms",
        "exposureTimeFirst": "exposure_time_first_ms",
        "exposureTimeCalibration": "calibrate_time_ms",
        "exposureUserProfile": "exposure_profile_by_id",
    }
    data = request.json

    if data:
        result = {}
        for name_out, name_in in convert.items():
            if name_out in data:
                result[name_in] = Variant("i", data[name_out])
        current_app.printer.sys.project_set_properties(result)
    return defines.NO_CONTENT

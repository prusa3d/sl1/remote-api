# -*- coding: utf-8 -*-
# This file is part of the SLA firmware
# Copyright (C) 2021 Prusa Research a.s. - www.prusa3d.com
# SPDX-License-Identifier: GPL-3.0-or-later

import os

from . import api

from flask import current_app, request

from remote_api_link import defines
from remote_api_link.exceptions import RemoteApiError
from remote_api_link.states import State


@api.route("/job", methods=["GET"])
def jobState():
    printer = current_app.printer
    state = State(printer.sys.state)
    state_text = state.get_flags()["text"]

    if state not in [State.READY, State.ERROR]:
        data = printer.sys.job
        job = {
            "file": {
                "name": os.path.basename(data["path"]),
                "layers": data["total_layers"],
                "exposureTime": data["exposureTime"],
                "exposureTimeFirst": data["exposureTimeFirst"],
                "exposureUserProfile": data["exposureUserProfile"],
            },
        }
        if "exposureTimeCalibration" in data:
            job["file"]["exposureTimeCalibration"] = data["exposureTimeCalibration"]

        if "previous_prints" not in data["path"]:
            data_source = printer.fs.get_metadata(data["path"], False)
            data_source = data_source["files"]
            origin = data_source["origin"]
            parent_path = printer.convert_path(
                origin, permite_other_sources=["previous-prints"]
            )
            job["file"].update(
                {
                    "path": os.path.relpath(data["path"], start=parent_path),
                    "date": data_source["mtime"],
                    "origin": origin,
                    "size": data_source["size"],
                }
            )
            config = data_source.get("metadata", {}).get("config", {})
            if "layerHeight" in config:
                job["file"]["layerHeight"] = config["layerHeight"]

        result = {
            "state": state_text,
            "job": job,
        }

        if state == State.PRINTING:
            result.update(
                {
                    "progress": {
                        "completion": data["progress"],
                        "printTimeLeft": data["remaining_time"],
                        "currentLayer": data["current_layer"],
                        "printTime": data["time_elapsed"],
                    },
                    "resin": {
                        "remaining": data["remaining_material"],
                        "consumed": data["consumed_material"],
                    },
                }
            )
        else:
            result.update(
                {
                    "progress": {
                        "printTimeLeft": data["remaining_time"],
                    },
                }
            )

        return result
    else:
        return {"state": state_text}


@api.route("/job", methods=["POST"])
def controlJob():
    command = None
    data = request.json
    if data:
        command = data.get("command")

    printer = current_app.printer
    if command == "start":
        printer.sys.cmd_confirm()
    elif command == "cancel":
        printer.sys.cmd_cancel()
    elif command == "pause":
        if data.get("action") == "resume":
            printer.sys.cmd_continue()
        elif data.get("action") == "pause":
            printer.sys.cmd_pause()  # this is used only to invoke feedme at the moment
        else:
            raise RemoteApiError("unknown pause action")
    else:
        raise RemoteApiError("unknown job command")

    return defines.NO_CONTENT

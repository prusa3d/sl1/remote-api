# This file is part of the SLA firmware
# Copyright (C) 2020 Prusa Research a.s. - www.prusa3d.com
# SPDX-License-Identifier: GPL-3.0-or-later

import re
import secrets
import subprocess  # nosec
from pathlib import Path
import setuptools.command.build_py
from setuptools import setup, find_packages

data_files = [
    ("/usr/lib/systemd/system", ["systemd/remote_api_link.service"]),
    ("/usr/lib/tmpfiles.d/", ["systemd/remote_api_link-tmpfiles.conf"]),
    ("/usr/lib/sysusers.d/", ["systemd/remote_api_link-user.conf"]),
]


METADATA = {}
with open("remote_api_link/__init__.py", "r") as info:
    METADATA = dict(re.findall(r'__([a-z_]+)__ = "([^"]+)"', info.read()))

with open("README.md", "r") as fh:
    long_description = fh.read()


class BuildPyWithLocalesCommand(setuptools.command.build_py.build_py):
    def run(self):
        subprocess.check_call(["make", "-C", "remote_api_link/locales"])  # nosec
        setuptools.command.build_py.build_py.run(self)

    config_file = Path(__file__).parent / "remote_api_link" / "config.cfg"
    with config_file.open(mode="w") as f:
        sec = secrets.token_hex(16)
        f.write(f"SECRET_KEY = b'{sec}'\n")


setup(
    name="remote_api_link",
    version=METADATA["version"],
    author=METADATA["author_name"],
    author_email=METADATA["author_email"],
    maintainer=METADATA["author_name"],
    maintainer_email=METADATA["author_email"],
    url=METADATA["url"],
    description=METADATA["description"],
    long_description=long_description,
    long_description_content_type="text/markdown",
    license="GNU General Public License v3 or later (GPLv3+)",
    packages=find_packages(),
    data_files=data_files,
    package_data={"remote_api_link": ["locales/*/LC_MESSAGES/*.mo", "config.cfg"]},
    cmdclass={"build_py": BuildPyWithLocalesCommand},
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)",
        "Operating System :: POSIX :: Linux",
    ],
    python_requires=">=3.6",
)

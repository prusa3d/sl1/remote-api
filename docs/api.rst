OpenAPI Specification
=====================

.. toctree::
   :maxdepth: 2

Api
---
The OpenAPI Specification file: openapi.yaml_.

.. _openapi.yaml: https://github.com/prusa3d/Prusa-Connect-Local/blob/master/spec/openapi.yaml

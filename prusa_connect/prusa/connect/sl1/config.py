from ast import literal_eval
from configparser import ConfigParser

from logging import getLogger

# TODO: currently supported only section service::connect
# Note: .ini files are hell!

_defaults = {
    "service::connect": {
        "hostname": "connect.prusa3d.com",
        "tls": True,
        "port": 0,
        "token": "",
    }
}


class Config:
    CONFIG_PATH = "/etc/sl1fw/prusa_printer_settings.ini"

    def __init__(self) -> None:
        self._logger = getLogger(__name__)
        self._config: ConfigParser = ConfigParser()
        self._config.read_dict(_defaults)
        self._config.read(self.CONFIG_PATH)
        self.connect = {
            "hostname": self._config.get(
                "service::connect", "hostname", fallback="connect.prusa3d.com"
            ),
            "tls": literal_eval(
                self._config.get("service::connect", "tls", fallback=True)
            ),
            "port": int(
                self._config.get("service::connect", "port", fallback=443) or 0
            ),
            "token": self._config.get("service::connect", "token", fallback=""),
        }

    @property
    def connect_server_url(self) -> str:
        protocol = "https://" if self.connect["tls"] else "http://"
        port = f":{self.connect['port']}" if self.connect["port"] != 0 else ""
        return f"{protocol}{self.connect['hostname']}{port}"

    def reset(self) -> None:
        self.connect["token"] = ""  # nosec bandit B105
        self.save()

    def save(self) -> None:
        for option, value in self.connect.items():
            self._config.set("service::connect", option, str(value))
        with open(self.CONFIG_PATH, "w") as f:
            self._config.write(f)
        self._logger.info("Config has been saved")

# This file is part of the SL1 firmware
# Copyright (C) 2020 Prusa Research a.s. - www.prusa3d.com
# SPDX-License-Identifier: GPL-3.0-or-later

import os
import signal
import logging
import base64
from time import sleep, time
import threading
from enum import Enum
from typing import Any, Dict
from hashlib import sha256
from gi.repository import GLib
from pydbus import SystemBus
from requests.exceptions import ConnectionError as SDKConnectionError
from urllib3.exceptions import HTTPError

from prusa.connect.printer import (
    Printer as SdkPrinter,
    Command as SDKCommand,
    const,
    files,
    command,
)

from slafw.api.standard0 import Standard0State
from prusaerrors.sl1.codes import Sl1Codes
from prusaerrors.shared.codes import Code

from .dbus import PrusaConnect0
from .errors import ConnectionFailedError
from .states import standard0_to_connect_state
from .dialog import Dialog
from .config import Config


def flatten(d: Dict) -> Dict[str, Any]:
    new_items = {}
    for key, value in d.items():
        if isinstance(value, dict):
            new_items.update(value)
        else:
            new_items[key] = value
    return new_items


WAIT_TIMEOUT = 0.2
FULL_TELEMETRY_SYNC_EVERY_SECS = 1

TELEMETRY_WHITELIST = [
    "transfer_id",
    "transfer_progress",
    "transfer_time_remaining",
    "transfer_transferred",
    "time_transferring",
]

SLATypes = Enum(
    value="Name",
    names=[
        ("Original Prusa SL1", const.PrinterType.SL1),
        ("Original Prusa SL1S SPEED", const.PrinterType.SL1S),
        ("Original Prusa Medical One", const.PrinterType.M1),
    ],
)


class Connect:
    def __init__(self):
        bus = SystemBus()

        self._printer = bus.get("cz.prusa3d.sl1.standard0")
        self._printer.onLastErrorOrWarn = self._error_occurred
        self._printer.onPropertiesChanged = self._properties_changed

        self._fs = bus.get("cz.prusa3d.sl1.filemanager0")
        self._fs.onMediaEjected = self._media_ejected
        self._fs.onPropertiesChanged = self._fs_properties_changed

        self._dbus_interface = PrusaConnect0(self)
        bus.publish(PrusaConnect0.__INTERFACE__, self._dbus_interface)

        self._logger = logging.getLogger(__name__)
        self._logger.info("Initializing Prusa Connect")

        self.config = Config()

        self._registration_status: const.RegistrationStatus = (
            const.RegistrationStatus.NO_REGISTRATION
        )

        self._is_ok = True
        self._usb_attached = False
        self._local_attached = False
        self._printer_registration_address = ""

        info = self._printer.info
        printer_type = SLATypes[info["name"]].value
        if printer_type:
            self._printer_type = printer_type
        else:
            self._logger.warn(f"Unsupported printer model: {info['name']}")

        self._pending_transfer = None
        self._connect_state: const.State = None
        self._stop_request = threading.Event()
        self._fingerprint = sha256(info["sn"].encode()).hexdigest()
        self._dialog: Dialog = None

        self._logger.info(f"serial {info['sn']}")
        self._logger.info(f"fingerprint: {self._fingerprint}")
        self._logger.info(f"firmware: {info['firmware']}")

        self._sdk_printer = SdkPrinter(
            type_=self._printer_type,
            sn=info["sn"],
            fingerprint=self._fingerprint,
            mmu_supported=False,
        )
        self._sdk_printer.network_info = {
            "lan_mac": info["eth_mac"],
            "lan_ipv4": self._printer.net_ip,
            "lan_ipv6": None,
            "wifi_mac": info["wlan_mac"],
            "wifi_ipv4": None,
            "wifi_ipv6": None,
            "wifi_ssid": None,
        }
        self._sdk_printer.firmware = info["firmware"]

        if self.config.connect["token"]:
            self.registration_status = const.RegistrationStatus.FINISHED

        if self.registration_status != const.RegistrationStatus.NO_REGISTRATION:
            self._sdk_printer.set_connection(
                self.config.connect_server_url, self.config.connect["token"]
            )

        self._sdk_printer.register_handler = self.register_handler
        self._sdk_printer.set_handler(
            const.Command.SEND_JOB_INFO, self.send_job_info_handler
        )
        self._sdk_printer.set_handler(const.Command.SEND_INFO, self.send_info_handler)
        self._sdk_printer.set_handler(
            const.Command.SEND_FILE_INFO, self.send_file_info_handler
        )
        self._sdk_printer.set_handler(
            const.Command.SEND_STATE_INFO, self.send_state_info_handler
        )
        self._sdk_printer.set_handler(
            const.Command.SEND_TRANSFER_INFO, self.send_transfer_info_handler
        )
        self._sdk_printer.set_handler(
            const.Command.START_CONNECT_DOWNLOAD, self.start_connect_download_handler
        )
        self._sdk_printer.set_handler(
            const.Command.START_PRINT, self.start_print_handler
        )
        self._sdk_printer.set_handler(const.Command.STOP_PRINT, self.stop_print_handler)
        self._sdk_printer.set_handler(
            const.Command.PAUSE_PRINT, self.pause_print_handler
        )
        self._sdk_printer.set_handler(
            const.Command.RESUME_PRINT, self.resume_print_handler
        )
        self._sdk_printer.set_handler(
            const.Command.RESET_PRINTER, self.reset_printer_handler
        )
        self._sdk_printer.set_handler(const.Command.DIALOG_ACTION, self.dialog_action)

        logging.getLogger("connect-printer").setLevel("DEBUG")  # log sdk
        logging.getLogger("urllib3.connectionpool").setLevel(
            "DEBUG"
        )  # log z python-requests

        self._telemetry = {}
        self._last_full_telemetry = time()

    def _media_mounted(self, *_):
        self._logger.info("USB inserted")
        if not self._usb_attached and self.config.connect["token"]:
            self._try_attach_usb()

    def _media_ejected(self, *_):
        self._logger.info("USB removed")
        if self._usb_attached:
            self._sdk_printer.detach("usb")
            self._usb_attached = False

    def _try_attach_usb(self):
        try:
            usb_root = self._fs.usb_mount_point
            if usb_root:
                self._logger.info(f"Attach USB FS: {usb_root}")
                self._sdk_printer.attach(usb_root, "usb")
                self._usb_attached = True
        except Exception as e:
            self._logger.info("Failed to attach USB")
            self._logger.exception(e)

    def _error_occurred(self, error: Dict[str, Any]):
        if error["code"]:
            if error["code"] in (
                Sl1Codes.NONE.code,
                Sl1Codes.NONE_WARNING.code,
            ):
                self._dialog = None
            else:
                code: Code = Sl1Codes.get(error["code"])
                self._dialog = Dialog(code, error)
                self.set_state(self._connect_state, const.Source.FIRMWARE)
        self._logger.error(f"Error occurred: {error}")

    @property
    def printer_registration_address(self):
        return self._printer_registration_address

    @printer_registration_address.setter
    def printer_registration_address(self, value: str):
        self._printer_registration_address = value
        self._logger.info(
            f"setting printer_registration_address {self._printer_registration_address}"
        )
        self._dbus_interface.refresh_properties()
        return self._printer_registration_address

    @property
    def registration_status(self) -> const.RegistrationStatus:
        self._logger.info(f"registration_status {self._registration_status.value}")
        return self._registration_status

    @registration_status.setter
    def registration_status(self, value: const.RegistrationStatus) -> None:
        self._registration_status = value
        self._logger.info(
            f"setting registration_status {self._registration_status.value}"
        )
        self._dbus_interface.refresh_properties()
        if value == const.RegistrationStatus.FINISHED:
            self._properties_changed("", "state", "")  # set initial printer state

    def set_server(self, hostname: str, port: int, tls: bool):
        self.config.connect["hostname"] = hostname
        self.config.connect["port"] = port
        self.config.connect["tls"] = tls
        self._sdk_printer.set_connection(
            self.config.connect_server_url, self.config.connect["token"]
        )
        self.registration_status = const.RegistrationStatus.IN_PROGRESS
        self.config.save()
        self._is_ok = True

    def register_handler(self, token):
        self.config.connect["token"] = token
        self._logger.info("Got token")
        self.printer_registration_address = ""
        self.registration_status = const.RegistrationStatus.FINISHED
        self.config.save()

    def signalHandler(self, _signal, _frame):
        self._logger.debug("signal received")
        # self._properties_changed_registration.unsubscribe()
        self._sdk_printer.stop_loop()
        self._stop_request.set()

    def main_loop(self):
        self._logger.info("start telemetry loop")
        while not self._stop_request.is_set():
            try:
                if self.config.connect["token"]:
                    # Local storage driven by SDK FS:
                    if self._local_attached:
                        self._sdk_printer.detach("local")

                    self._logger.info(
                        f"Attach LOCAL filesystem: {self._fs.convert_path('local', '')}"
                    )
                    self._sdk_printer.attach(
                        self._fs.convert_path("local", ""), "local"
                    )
                    self._local_attached = True

                    # USB storage driven by SDK
                    if not self._usb_attached:
                        self._try_attach_usb()

                    while (
                        self.config.connect["token"] and not self._stop_request.is_set()
                    ):
                        telemetry = self.telemetry()

                        if len(telemetry.keys()) != 0:
                            self._sdk_printer.telemetry(**telemetry)
                        self._is_ok = True
                        sleep(1)

            except SDKConnectionError as error:
                self._is_ok = False
                self._logger.error(
                    "Failed to establish a new connection [errno %d]", error.errno
                )
            except HTTPError as error:
                self._is_ok = False
                if hasattr(error, "message"):
                    self._logger.warning(getattr(error, "message"))
                sleep(FULL_TELEMETRY_SYNC_EVERY_SECS)
            finally:
                sleep(FULL_TELEMETRY_SYNC_EVERY_SECS)

    def inotify_loop(self):
        """Inotify_handler in loop."""
        while not self._stop_request.is_set():
            try:
                self._sdk_printer.inotify_handler()
                sleep(0.2)
            except Exception as e:  # pylint: disable=broad-except
                self._logger.error("Inotify handler error")
                self._logger.exception(e)

    def handle_commands(self):
        self._logger.info("Starting command handler thread")
        while not self._stop_request.is_set():
            if self._sdk_printer.command.new_cmd_evt.wait(WAIT_TIMEOUT):
                self._sdk_printer.command()

    def _properties_changed(self, __, changed, ___):
        if self.config.connect["token"] and "state" in changed:
            standard0_state = self._printer.state
            self._connect_state = standard0_to_connect_state(standard0_state)
            if self._connect_state == const.State.ATTENTION:
                self._dialog = Dialog(Sl1Codes.FILL_THE_RESIN)
            elif self._connect_state != const.State.ERROR:
                self._dialog = None
            self.set_state(self._connect_state, const.Source.FIRMWARE)

    def _fs_properties_changed(self, _, changed, __):
        if "media_mounted" in changed:
            if changed["media_mounted"]:
                self._media_mounted()

    def is_ok(self) -> bool:
        return self._is_ok

    def register(self) -> str:
        try:
            code = self._sdk_printer.register()
            self._is_ok = True
            registration_address = f"{self.config.connect_server_url}/add-printer/connect/{self._printer_type}/{code}"
            self.printer_registration_address = registration_address
            return registration_address
        except SDKConnectionError:
            self._is_ok = False
            raise ConnectionFailedError()

    def unregister(self):
        self.config.reset()
        self.registration_status = const.RegistrationStatus.NO_REGISTRATION

    def register_confirm(self, temp_token: str) -> bool:
        """Confirm the register.

        Args:
            temp_token (str): Token get in net_sdk_printer_register()

        Raises:
            Exception: when there isn't a instance of Prusa connect
                       It has to call net_sdk_printer_register() first.

        Returns:
            bool: confirm if the register was finished
        """
        try:
            printer_token = self._sdk_printer.get_token(temp_token)
            if printer_token:
                self.config.connect["token"] = printer_token
                self.config.save()
                self._logger.info("Printer registered.")
                return True
        except SDKConnectionError:
            raise ConnectionFailedError()

        return False

    def telemetry(self):
        hw_telemetry = self._printer.hw_telemetry
        temperatures = hw_telemetry["temperatures"]
        fans = hw_telemetry["fans"]
        now = time()

        if now - self._last_full_telemetry > FULL_TELEMETRY_SYNC_EVERY_SECS:
            self._telemetry = {}
            self._last_full_telemetry = now

        result = {
            "temp_uv_led": temperatures["temp_led"],
            "temp_cpu": temperatures["cpu_temp"],
            "temp_ambient": temperatures["temp_amb"],
            "fan_uv_led": fans["uv_led"],
            "fan_blower": fans["blower"],
            "fan_rear": fans["rear"],
            "target_fan_rear": fans["rear_target"],
            "cover_closed": hw_telemetry["cover_closed"],
        }

        transfer = self._fs.transfer
        transfer_id = transfer.get("id", None)
        if transfer:
            result["transfer_id"] = transfer_id
            result["transfer_progress"] = transfer.get("progress", 0)
            result["transfer_time_remaining"] = transfer.get("time_remaining", 0)
            result["transfer_transferred"] = transfer.get("size_transferred", 0)
            result["time_transferring"] = transfer.get("time_elapsed", 0)

        if self._pending_transfer and transfer_id != self._pending_transfer["id"]:
            self._sdk_printer.event_cb(
                const.Event.TRANSFER_FINISHED,
                const.Source.CONNECT,
                path=self._pending_transfer["path"],
                transfer_id=self._pending_transfer["id"],
                start_cmd_id=self._pending_transfer["start_cmd_id"],
            )
            self._pending_transfer = None

        if self._connect_state == const.State.PRINTING:
            job = self._printer.job
            current_layer = job["current_layer"]
            result["time_remaining"] = int(job["remaining_time"])

            if self._telemetry.get("current_layer") != current_layer:
                result["time_printing"] = int(job["time_elapsed"])
                result["current_layer"] = current_layer
                result["resin_consumed"] = job["consumed_material"]

            result["job_id"] = job["instance_id"]
            result["progress"] = round(job["progress"] * 100)

            result["exposure_time"] = job["exposureTime"] / 1000
            result["exposure_time_first"] = job["exposureTimeFirst"] / 1000

            if "exposureTimeCalibration" in job:
                result["exposure_time_calibration"] = (
                    job["exposureTimeCalibration"] / 1000
                )

            if job["remaining_material"] != -1:
                result["resin_remaining"] = job["remaining_material"]

            result["resin_low"] = job["resin_low"]

        if self._dialog:
            result["dialog_id"] = self._dialog.dialog_id

        for key in list(result.keys()):
            if key in self._telemetry:
                if key in TELEMETRY_WHITELIST:
                    continue
                if self._telemetry[key] != result[key]:
                    if key in ["temp_uv_led", "temp_cpu", "temp_ambient"]:
                        if not abs(self._telemetry[key] - result[key]) < 0.5:
                            continue
                    else:
                        continue

                del result[key]

        self._telemetry.update(result)

        return result

    def send_job_info_handler(self, caller: SDKCommand):
        if self._printer.state != Standard0State.PRINTING.value:
            return {"job_id": 0}

        result = {}
        try:
            job = self._printer.job
            data_source = self._fs.get_metadata(job["path"], False)
            data_source = data_source["files"]
            origin = data_source["origin"]

            storage_path = self._fs.convert_path(origin, "")
            path = os.path.join(
                "/",
                origin,
                os.path.relpath(data_source["path"].lstrip("/"), start=storage_path),
            )

            result = {
                "job_id": job["instance_id"],
                "state": self._connect_state,
                "event": const.Event.JOB_INFO,
                "source": const.Source.CONNECT,
                "time_printing": job["time_elapsed"],
                "time_remaining": job["remaining_time"],
                "progress": round(job["progress"] * 100),
                "file_path": path,
                "m_timestamp": data_source["mtime"] or 0,
                "size": data_source["size"],
                "path_incomplete": False,
            }
        except Exception as e:
            self._logger.info("job error")
            self._logger.exception(e)

        return result

    def send_info_handler(self, caller: SDKCommand):
        result = self._sdk_printer.get_info()
        config = self._printer.config

        result["cover_check"] = config["cover_check"]
        result["is_calibrated"] = config["is_calibrated"]
        result["auto_off"] = config["auto_off"]
        result["resin_sensor"] = config["resin_sensor"]

        NON_SLA_KEYS = ("mmu",)
        for key in NON_SLA_KEYS:
            if key in result:
                del result[key]

        return result

    def split_file_path(self, path: str) -> (str, str):
        parts = path.lstrip("/").split("/")
        storage = parts.pop(0)
        return storage, "/".join(parts)

    def send_file_info_handler(self, caller: SDKCommand):
        if not caller.kwargs or "path" not in caller.kwargs:
            raise ValueError("SEND_FILE_INFO requires kwargs")

        path = caller.kwargs["path"]

        (storage, file_in_storage_path) = self.split_file_path(path)
        storage_path = self._fs.convert_path(storage, "")
        file_path = os.path.join("/", storage_path, file_in_storage_path)

        file_info = self._fs.get_from_path(file_path, 1)["files"]

        if not file_info["exists"]:
            raise ValueError(f"File does not exist: {file_path}")

        info = {
            "source": const.Source.CONNECT,
            "event": const.Event.FILE_INFO,
            "path": path,
            "read_only": False,
            "ro": False,
            "m_timestamp": file_info["mtime"],
        }
        info["size"] = file_info["size"] if "size" in file_info else 0

        if "metadata" not in file_info:
            get_metadata = self._fs.get_metadata(file_path, True)
            if "metadata" not in get_metadata["files"]:
                return info
            file_info["metadata"] = get_metadata["files"]["metadata"]

        # parse metadata
        config = file_info["metadata"].get("config", {})
        # copy all items from config to info
        info.update(config)
        # append total_layers and total_height to info
        if "numFast" in config and "numSlow" in config:
            info["total_layers"] = config["numFast"] + config["numSlow"]
            if "layerHeight" in config:
                info["total_height"] = info["total_layers"] * config["layerHeight"]
        # append base64 preview to info
        if "thumbnail" in file_info["metadata"]:
            thumbnail = file_info["metadata"]["thumbnail"]
            if "files" in thumbnail:
                for preview_path in thumbnail["files"]:
                    if "400x400" in preview_path:
                        with open(preview_path, "rb") as preview_file:
                            info["preview"] = base64.b64encode(
                                preview_file.read()
                            ).decode("utf-8")
        return info

    def send_state_info_handler(self, caller: SDKCommand):
        info = {
            "source": const.Source.CONNECT,
            "event": const.Event.STATE_CHANGED,
            "state": self._connect_state,
        }
        if self._dialog:
            info.update(vars(self._dialog))

        return info

    def send_transfer_info_handler(self, caller: SDKCommand):
        """Provide info of the running transfer"""
        kwargs = caller.kwargs or {}
        command_transfer_id = kwargs.get("transfer_id")

        transfer = self._fs.transfer
        transfer_id = transfer.get("id") if transfer else None

        if command_transfer_id and transfer_id != command_transfer_id:
            raise command.CommandFailed("Not current transfer.")

        info = {
            "source": const.Source.CONNECT,
            "event": const.Event.TRANSFER_INFO,
        }

        if transfer_id is not None:
            info["progress"] = transfer.get("progress", 0)
            info["time_remaining"] = transfer.get("time_remaining", 0)
            info["transferred"] = transfer.get("size_transferred", 0)
            info["transfer_id"] = transfer_id
            info["type"] = transfer.get("type")
            info["size"] = transfer.get("size_total")
            info["path"] = transfer.get("path")
            info["to_print"] = transfer.get("to_print", False)
            info["to_select"] = transfer.get("to_select", False)

        return info

    def start_connect_download_handler(self, caller: SDKCommand):
        """Download a printable file from Connect, compose an URL using
        Connect config"""

        if not caller.kwargs:
            raise ValueError(f"{const.Command.START_CONNECT_DOWNLOAD} requires kwargs")

        try:
            uri = "/p/teams/{team_id}/files/{hash}/raw".format(**caller.kwargs)
            (storage, file_in_storage_path) = self.split_file_path(
                caller.kwargs["path"]
            )
            transfer_id = self._fs.download_from_url(
                self.config.connect_server_url + uri,
                storage,
                file_in_storage_path,
                const.TransferType.FROM_CONNECT.value,
                True,
                self._sdk_printer.make_headers(),
                caller.command_id,
            )
            self._pending_transfer = self._fs.transfer
            return {
                "source": const.Source.CONNECT,
                "transfer_id": transfer_id,
                "start_cmd_id": caller.command_id,
                "hash": caller.kwargs["hash"],
            }
        except KeyError as err:
            raise ValueError(
                f"{const.Command.START_CONNECT_DOWNLOAD} requires " f"kwarg {err}."
            ) from None

    def start_print_handler(self, caller: SDKCommand):
        if not caller.kwargs or "path" not in caller.kwargs:
            raise ValueError("START_PRINT requires kwargs")

        path = caller.kwargs["path"]
        state = self._printer.state

        (storage, file_in_storage_path) = self.split_file_path(path)
        storage_path = self._fs.convert_path(storage, "")

        if storage not in ["local", "usb"]:
            raise ValueError(f"Unsupported storage: {storage}")

        try:
            if state == Standard0State.SELECTED.value:
                self._printer.cmd_cancel()

            real_path = os.path.join(storage_path, file_in_storage_path)
            self._fs.get_from_path(real_path, 0)  # check if exist

            auto_advance = True
            ignore_errors = False
            self._printer.cmd_select(real_path, auto_advance, ignore_errors)
            return {"source": const.Source.CONNECT}
        except GLib.Error as e:
            self._sdk_printer.cancel_printer_ready(caller)
            return {
                "source": const.Source.WUI,
                "event": const.Event.REJECTED,
                "message": e.message,
                "reason": "NotImplemented",
            }

    def stop_print_handler(self, caller: SDKCommand):
        self._printer.cmd_cancel()
        return {"source": const.Source.CONNECT}

    def pause_print_handler(self, caller: SDKCommand):
        self._printer.cmd_pause()
        return {"source": const.Source.CONNECT}

    def resume_print_handler(self, caller: SDKCommand):
        self._printer.cmd_continue()
        return {"source": const.Source.CONNECT}

    def reset_printer_handler(self, caller: SDKCommand):
        self._printer.reset_printer()
        return {"source": const.Source.CONNECT}

    def dialog_action(self, caller: SDKCommand):
        button = caller.kwargs["button"]
        state = self._printer.state

        if button == "Resin filled":
            if state is Standard0State.POUR_IN_RESIN.value:
                self._printer.cmd_continue()
            elif state is Standard0State.FEED_ME.value:
                self._printer.cmd_resin_refill()
                self._printer.cmd_continue()
            elif state is Standard0State.BUSY.value:
                self._printer.cmd_confirm_print_warning()
            else:
                raise ValueError("Unknown state")
        elif button == "Cancel print":
            if state in [
                Standard0State.POUR_IN_RESIN.value,
                Standard0State.FEED_ME.value,
                Standard0State.ERROR.value,
            ]:
                self._printer.cmd_cancel()
            elif state is Standard0State.BUSY.value:
                self._printer.cmd_reject_print_warning()
            else:
                raise ValueError("Unknown state")
        else:
            raise ValueError("Unknown button")

        self._dialog = None
        return {"source": const.Source.CONNECT}

    def get_local_storage(self):
        return self.get_storage("local")

    def get_usb_storage(self):
        return self.get_storage("usb")

    def get_storage(self, storage_name):
        storage = files.File(storage_name, is_dir=True, read_only=False, ro=False)
        storage_path = self._fs.convert_path(storage_name, "")
        data_source = self._fs.get_from_path(storage_path, -1)

        for child in data_source["files"]["children"]:
            self.map_storage(
                storage,
                storage_path,
                child,
                data_source["last_update"][storage_name],
                recursive=True,
            )

        return storage

    def run(self):
        self._logger.info("Starting Prusa Connect")
        signal.signal(signal.SIGINT, self.signalHandler)
        signal.signal(signal.SIGTERM, self.signalHandler)

        loop = GLib.MainLoop()
        threading.Thread(target=loop.run, daemon=True).start()
        threading.Thread(target=self.main_loop).start()
        threading.Thread(target=self.handle_commands).start()
        threading.Thread(target=self.inotify_loop).start()
        self._sdk_printer.loop()
        loop.quit()

    def map_type(self, filetype, filename):
        if filetype == "folder":
            return const.FileType.FOLDER
        (_, extension) = os.path.splitext(filename)

        if extension in self._fs.extensions:
            return const.FileType.PRINT_FILE

        if extension == "xz":
            return const.FileType.FIRMWARE

        return const.FileType.FILE

    def map_storage(
        self,
        storage: files.File,
        storage_path,
        file_info,
        last_update=None,
        recursive=False,
    ):
        origin = file_info["origin"]
        path = file_info["path"]
        rel_path = os.path.relpath(path, start=storage_path)
        virtual_path = os.path.abspath(os.path.join("/", origin, rel_path))
        name = os.path.basename(path)
        file_type = self.map_type(file_info["type"], name)
        is_dir = file_type == const.FileType.FOLDER

        attrs = {
            "path": os.path.dirname(virtual_path),
            "read_only": False,
            "ro": False,
            "m_timestamp": (
                file_info["mtime"] if file_info["mtime"] != 0 else last_update
            ),
        }

        if "size" in file_info:
            attrs["size"] = file_info["size"]

        file = storage.add(name, is_dir=is_dir, type=file_type, **attrs)
        if is_dir and recursive and "children" in file_info:
            folder_path = os.path.join(storage_path, name)
            for child in file_info["children"]:
                self.map_storage(
                    file,
                    folder_path,
                    child,
                    last_update=None,
                    recursive=recursive,
                )

    def set_state(self, state: const.State, source: const.Source):
        if self._dialog:
            self._sdk_printer.set_state(
                state,
                source,
                dialog_id=self._dialog.dialog_id,
                code=self._dialog.code,
                title=self._dialog.title,
                text=self._dialog.text,
                buttons=self._dialog.buttons,
                reason=self._dialog.reason,
                placeholders=self._dialog.placeholders,
                values=self._dialog.values,
            )
        else:
            self._sdk_printer.set_state(state, source)

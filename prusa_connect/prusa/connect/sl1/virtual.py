import logging
from unittest.mock import patch, Mock

from prusa.connect.sl1.__main__ import main

logging.basicConfig(
    format="%(asctime)s - %(levelname)s - %(name)s - %(message)s", level=logging.DEBUG
)

# log into current terminal instead of journalctl
with patch("prusa.connect.sl1.__main__.init_logging", Mock):
    main()

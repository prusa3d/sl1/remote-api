from prusaerrors.shared.codes import Code

from typing import Any, Dict, List
import re


class Dialog:
    _dialog_id = 1

    def __init__(self, code: Code, error: Dict[str, Any] = None):
        self.dialog_id = Dialog._dialog_id
        Dialog._dialog_id += 1

        self.code: int = code.raw_code
        self.title: str = code.title
        self.reason: str = code.title
        self.buttons: List[str] = None
        self.placeholders: Dict[str, int] = {}
        self.values: Dict[str, int] = {}

        if code.action:
            self.buttons = code.action

        self.text: str = code.message
        if error:
            if "fan__map_HardwareDeviceId" in error:
                id = error["fan__map_HardwareDeviceId"]
                self.placeholders["fan__map_HardwareDeviceId"] = id
                if id == 2000:
                    error["fan__map_HardwareDeviceId"] = "UV LED fan"
                elif id == 2001:
                    error["fan__map_HardwareDeviceId"] = "blower fan"
                elif id == 2002:
                    error["fan__map_HardwareDeviceId"] = "rear fan"
            if "sensor__map_HardwareDeviceId" in error:
                id = error["sensor__map_HardwareDeviceId"]
                self.placeholders["sensor__map_HardwareDeviceId"] = id
                if id == 1000:
                    error["sensor__map_HardwareDeviceId"] = "UV LED temperature"
                elif id == 1001:
                    error["sensor__map_HardwareDeviceId"] = "ambient temperature"
                elif id == 1002:
                    error["sensor__map_HardwareDeviceId"] = "CPU temperature"
            formatted_text = re.sub(
                r"%\(([^)]+)\)(?:\.\d+)?[a-zA-Z]", r"{\1}", code.message
            )
            self.text: str = formatted_text.format(**error)
            for key, value in error.items():
                if key not in (
                    "code",
                    "name",
                    "text",
                    "fan__map_HardwareDeviceId",
                    "sensor__map_HardwareDeviceId",
                ):
                    self.values[key] = value

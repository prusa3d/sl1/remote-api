from prusa.connect.printer.const import State as ConnectState
from slafw.api.standard0 import Standard0State


def standard0_to_connect_state(state) -> ConnectState:
    """
    Translate state from cz.prusa32.sl1.standard0 to Prusa Connect
    """

    mapping = {
        Standard0State.READY: ConnectState.IDLE,
        Standard0State.SELECTED: ConnectState.IDLE,
        Standard0State.PRINTING: ConnectState.PRINTING,
        Standard0State.POUR_IN_RESIN: ConnectState.ATTENTION,
        Standard0State.BUSY: ConnectState.BUSY,
        Standard0State.FEED_ME: ConnectState.ATTENTION,
        Standard0State.ERROR: ConnectState.ERROR,
        Standard0State.FINISHED: ConnectState.FINISHED,
        Standard0State.STOPPED: ConnectState.STOPPED,
    }
    return mapping.get(Standard0State(state), ConnectState.ERROR)
